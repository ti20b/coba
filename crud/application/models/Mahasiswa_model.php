<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa_model extends CI_Model {

    public function ambilData()
    {
        $query = $this->db->get("anggota_2020310059");
        return $query->result();
    }

    public function tambahData($data)
    {
        $query = $this->db->insert("anggota_2020310059", $data);
    }

    public function hapusData($id)
    {
        $data = array(
            "id" => $id
        );
        $query = $this->db->delete("anggota_2020310059", $data);
    }

    public function editData($id)
    {
        $data = array(
            "id" => $id
        );
        $query = $this->db->get_where("anggota_2020310059", $data)->row();
    }

}

/* End of file Buku_model.php */
