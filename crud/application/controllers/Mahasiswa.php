<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->model('mahasiswa_model');

    }

    public function index()
    {
        $this->load->model("Mahasiswa_model", "mahasiswa");
        $data_mahasiswa = $this->mahasiswa->ambilData();

        $data = array(
            "mahasiswa" => $data_mahasiswa,
        );

        $this->load->view("index_mahasiswa", $data);
    }
    
    public function tambah()
    {
        $this->load->view("tambah_mahasiswa");
    }

    public function simpan_data()
    {
        $npm = $this->input->post("npm");
        $nama = $this->input->post("nama");
        $kelas = $this->input->post("kelas");
        $jurusan = $this->input->post("jurusan");

        $data = array(
            "npm" => $npm,
            "nama" => $nama,
            "kelas" => $kelas,
            "jurusan" => $jurusan
        );

        $this->load->model("Mahasiswa_model", "mahasiswa");
        $this->mahasiswa->tambahData($data);
        
        redirect('mahasiswa','refresh');
    }

    public function ubah($id)
    {
        $data['mahasiswa']=$this->mahasiswa_model->editData('mahasiswa');
        $this->load->view("edit_mahasiswa",$data);
    }

    public function simpan_ubah()
    {
        $npm = $this->input->post("npm");
        $nama = $this->input->post("nama");
        $kelas = $this->input->post("kelas");
        $jurusan = $this->input->post("jurusan");

        $data = array(
            "npm" => $npm,
            "nama" => $nama,
            "kelas" => $kelas,
            "jurusan" => $jurusan
        );

        $id = $this->input->post('id');
        $this->db->where('id', $id);
        $this->db->update('mahasiswa',$data);

        redirect('mahasiswa','refresh');
    }

    public function hapus($id)
    {
        $this->load->model("Mahasiswa_model", "mahasiswa");
        $this->mahasiswa->hapusData($id);
        
        redirect('mahasiswa','refresh');
    }

}

/* End of file Mahasiswa.php */
