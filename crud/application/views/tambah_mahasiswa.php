<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Tambah Anggota</title>
</head>
<body>
    <div class="container">
        <h1>Tambah Anggota</h1>
        <form action="<?php echo base_url('mahasiswa/simpan_data');?>" method="POST">
            <div class="form-group">
                <label for="judul">NPM</label>
                <input type="text" class="form-control" id="npm" name="npm" placeholder="Isikan NPM">
            </div>
            <div class="form-group">
                <label for="judul">Nama</label>
                <input type="text" class="form-control" id="nama" name="nama" placeholder="Isikan nama mahasiswa">
            </div>
            <div class="form-group">
                <label for="judul">Kelas</label>
                <input type="text" class="form-control" id="kelas" name="kelas" placeholder="Isikan kelas">
            </div>
            <div class="form-group">
            <label for="judul">Jurusan</label>
                <select class="form-control" id="jurusan" name="jurusan">
                    <option>Teknik Informatika</option>
                    <option>Sistem Informasi</option>
                    <option>Manajemen Informatika</option>
                <select/>
            </div>
            <div class="form-group">
                <br/>
                <input type="submit" class="btn btn-success" value="Simpan">
            </div>
        </form>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>