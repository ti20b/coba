<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <title>Data Anggota</title>
</head>
<body>
    <div class="container">
        <h1>Data Anggota - <a class="btn btn-primary" href="<?php echo base_url('mahasiswa/tambah'); ?>">Tambah</a></h1>
        <table class="table table-hover">
            <tr>
                <td>No</td>
                <td>NPM</td>
                <td>Nama</td>
                <td>Kelas</td>
                <td>Jurusan</td>
                <td>Aksi</td>
            </tr>
            <?php
                $nomor = 0;
                foreach($hasil as $item) {
                    $nomor++;
                    $id = $r->id;?>
                    <tr>
                        <td><?php echo $nomor; ?></td>
                        <td><?php echo $r->npm; ?></td>
                        <td><?php echo $r->nama; ?></td>
                        <td><?php echo $r->kelas; ?></td>
                        <td><?php echo $r->jurusan; ?></td>
                        <td>                           
                            <a class="btn btn-warning" href="<?php echo base_url('mahasiswa/ubah/').$id;?>">Ubah</a>
                            <a class="btn btn-danger" href="<?php echo base_url('mahasiswa/hapus/').$id; ?>">Hapus</a>
                        </td>
                    </tr>
                    <?php
                }
            ?>
        </table>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
</body>
</html>
